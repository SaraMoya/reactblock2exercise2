import React from 'react';
import './App.css';
import NamesChild from './namesChild.js';
import LastNamesChild from './lastNamesChild.js';

class App extends React.Component {
  render() {
//     Create a parent class component inside which you define two arrays of equal length, the first will have 5 firstnames and the second 5 lastnames.

// Create 2 children class components – one to display the firstnames and one to display the lastnames so that they are rendered side by side with matching order.
    const firstNames = ["Ana", "Fabio", "Andrea", 'Nora'];
    const lastNames = ["Sapkowski", "Chalotra", "Pendragon", 'Swan',];

    return (
      <div>
        {firstNames.map( (ele,i) => 
          <div>
            <NamesChild firstName={ele} />
            <LastNamesChild lastName={lastNames[i]} />
          </div>
          
        )}
        
    
      </div>
    );
  }
}

export default App;
